var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.nanoStreamH5Live = youbora.Adapter.extend({

  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.playhead
  },

  /** Override to return Frames Per Second (FPS) */
  getFramesPerSecond: function () {
    return this.framerate
  },

  /** Override to return dropped frames since start */
  getDroppedFrames: function () {
    return this.droppedframes
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    return this.bitrate
  },

  /** Override to return rendition */
  getRendition: function () {
    var ret = null
    if (this.width && this.height) {
      ret = youbora.Util.buildRenditionString(this.width, this.height)
    }
    return ret
  },

  /** Override to return title */
  getTitle: function () {
    var ret = null
    var config = this._getRealPlayerConfig()
    if (config && config.source && config.source.h5live && config.source.h5live.rtmp) {
      ret = config.source.h5live.rtmp.streamname
    }
    return ret
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function () {
    return true
  },

  /** Override to return resource URL. */
  getResource: function () {
    var ret = this.source
    var config = this._getRealPlayerConfig()
    if (config && config.url) {
      ret = config.url
    }
    return ret
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    return this.player.version
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'nanoStreamH5Live-' + this.player.type
  },

  _getRealPlayerConfig: function () {
    var ret = null
    if (this.player && this.player._core && this.player._core._realPlayer) {
      ret = this.player._core._realPlayer.config
    }
    return ret
  },

  registerListeners: function () {
    this.monitorPlayhead(true, false, 1200) // buffers, but no seeks (live has no seeks)
  },

  unregisterListeners: function () {
    if (this.monitor) this.monitor.stop() // buffers, but no seeks (live has no seeks)
  },

  fireEvent: function (name, e) {
    switch (name) {
      case 'onPlay':
        this.fireJoin()
        this.fireResume()
        break
      case 'onPause':
        this.firePause()
        break
      case 'onLoading':
        this.fireStart()
        break
      case 'onError':
        var ignoredCodes = [
          1007 // Playback suspended by external reason
        ]
        if (ignoredCodes.indexOf(e.data.code) === -1) {
          this.fireError(e.data.code, e.data.message)
          if (this.flags.isJoined) {
            this.fireStop()
          }
        }
        break
      case 'onStats':
        var stats = e.data.stats
        this.bitrate = stats.bitrate.current
        if (stats.currentTime !== this.playhead && stats.currentTime !== 0) {
          this.fireJoin()
          this.fireStart()
        }
        this.playhead = stats.currentTime
        this.framerate = stats.framerate.current
        this.droppedframes = stats.quality.droppedvideoFrames
        break
      case 'onStreamInfo':
        var stats2 = e.data.streamInfo
        this.height = stats2.videoInfo.height
        this.width = stats2.videoInfo.width
        this.source = stats2.url
        break
    }
  }

})

module.exports = youbora.adapters.NanoPlayer
