## [6.7.4] - 2020-11-11
### Library
- Packaged with `lib 6.7.22`

## [6.7.3] - 2020-10-29
### Added
- Simplified code for getTitle and getResource methods 
### Library
- Packaged with `lib 6.7.21`

## [6.7.2] - 2020-09-03
### Library
- Packaged with `lib 6.7.16`

## [6.7.1] - 2020-05-14
### Library
- Packaged with `lib 6.7.6`

## [6.7.0] - 2020-03-30
### Removed
- Bitrate from renditions
### Added
- View start in foreground after closing going to background
### Library
- Packaged with `lib 6.7.0`

## [6.5.1] - 2019-08-28
### Added
- Error retries accumulated in the same view
### Library
- Packaged with `lib 6.5.14`

## [6.5.0] - 2019-07-02
### Library
- Packaged with `lib 6.5.5`
- Extending from Adapter

## [6.4.1] - 2019-05-30
### Library
- Packaged with `lib 6.4.30`

## [6.4.0] - 2018-10-26
### Library
- Packaged with `lib 6.4.8`
